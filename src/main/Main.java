package main;

import java.util.Scanner;
import java.io.*;
import java.util.InputMismatchException;

public class Main {
	public static void main(String[] args){
		try{
			Scanner sc = new Scanner(System.in); //az objektumunk a billenty�zetr�l fogja beolvasni az �ltalunk megadott adatokat
		    int number;
		    number = sc.nextInt();
		    sc.close();
		    System.out.println("A bekert szam ketszerese: " + number*2);
		 }catch(InputMismatchException e){
		    System.out.println("Please give me a number");
		 }
	}
}
